const express = require('express');
const passport = require('passport');
const auth = require('../middlewares/auth');
const UserController = require('../controllers/userController');
const userValidator = require('../middlewares/validators/userValidator');

const router = express.Router();

router.post('/signup', [userValidator.signup, function(req, res, next) {
    passport.authenticate('signup', {
      session: false
    }, function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        res.status(401).json({
          status: 'Error',
          message: info.message
        });
        return;
      }
      UserController.signup(user, req, res, next);
    })(req, res, next);
  }]);
  router.post('/signin', [userValidator.signin, function(req, res, next) {
    passport.authenticate('signin', {
      session: false
    }, function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        res.status(401).json({
          status: 'Error',
          message: info.message
        });
        return;
      }
      UserController.signin(user, req, res, next);
    })(req, res, next);
  }]);
  router.post('/:id',UserController.getOne);
    

  
module.exports = router;
