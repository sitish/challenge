const express = require('express'); 
const router = express.Router();
const passport = require('passport'); 
const auth = require('../middlewares/auth');
const TicketController = require('../controllers/ticketController'); 
const ticketValidator=require('../middlewares/validators/ticketValidator');

router.get('/getAll',passport.authenticate('admin', {
    session: false
}),TicketController.getAll);
router.get('/:id',TicketController.getOne);
router.post('/create',passport.authenticate('user', {
    session: false
}), TicketController.create);
router.put('/closing/:id',passport.authenticate('admin', {
    session: false
}),TicketController.closing);
router.delete('/delete/:id',TicketController.delete);

module.exports = router; // export router