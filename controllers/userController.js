const { user } = require('../models');
const jwt = require('jsonwebtoken');

class UserController {
  async signup(user,req, res) {
    try {
      let body = {
        id: user.id,
        email: user.email
      };

      let token = jwt.sign({
        user: body
      }, 'secret_password');

      return res.status(200).json({
        message: "Signup success!",
        token: token
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        errors: e
      })
    }
  }
  async signin(user,req, res) {
    try {
      let body = {
        id: user.id,
        email: user.email
      };

      let token = jwt.sign({
        user: body
      }, 'secret_password');

      return res.status(200).json({
        message: "Signin success!",
        token: token
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        errors: e
      })
    }
  }
  async getOne(req, res) {
    try {
      let data = await user.findOne({
        where:{
          id:req.params.id
        }
      })

      return res.status(200).json({
        message: "success",
        data: data
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        errors: e
      })
    }
  }

  async failed(req, res) {
    try {
      return res.status(500).json({
        message: "Internal Server Error"
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        errors: e
      });
    }
  }

}

module.exports = new UserController;
