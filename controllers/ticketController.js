const {
    user,
    ticket
} = require("../models")

class TicketController {

    constructor() {
        user.hasMany(ticket, {
            foreignKey: 'id_user'
        })
        ticket.belongsTo(user, {
            foreignKey: 'id_user'
        })
    }

    async getAll(req, res) {
        try {

            const data = await ticket.findAll({ // find all data of Transaksi table
                include: [{
                    model: user,
                    attributes: ['name','image'] // just this attrubute from Barang that showed
                }]
            })
            return res.status(200).json({
                message: "success",
                data: data
            })


        } catch (error) {
            return res.status(500).json({
                message: "Internal Server Error",
                errors: error
            })
        }
    }

    async getOne(req, res) {
        try {

            const data = await ticket.findAll({ // find all data of Transaksi table
                where:{id:req.params.id},
                include: [{
                    model: user,
                    attributes: ['name','image'] // just this attrubute from Barang that showed
                }]
            })
            return res.status(200).json({
                message: "success",
                data: data
            })


        } catch (error) {
            return res.status(500).json({
                message: "Internal Server Error",
                errors: error
            })
        }
    }
    async create(req, res) {
        try {
            const dataTicket = await ticket.create({
                id_user: req.user.id,
                name: req.body.name,
                description: req.body.description,
                due_date: req.body.due_date,
                priority: req.body.priority,
                status: "pending"
            })
            return res.status(201).json({
                message: "success",
                data: dataTicket
            })

        } catch (error) {
            return res.status(500).json({
                message: "Internal Server Error",
                errors: error
            })
        }
    }

    async closing(req,res){
        try {
             await ticket.update({
                status:"closed"
            },{
                where:{
                    id:req.params.id
                }
            })
            const data=await ticket.findOne({
                where:{
                    id:req.params.id
                }
            })
            return res.status(201).json({
                message: "success",
                data: data
            })
            
        } catch (error) {
            // console.log(error);
            return res.status(500).json({
                message: "Internal Server Error",
                errors: error
            })
        }
    }

    async delete(req, res) {
        try {

            const data = await ticket.destroy({ // find all data of Transaksi table
                where:{id:req.params.id},
                
            })
            return res.status(200).json({
                message: "success",
                data: data
            })


        } catch (error) {
            return res.status(500).json({
                message: "Internal Server Error",
                errors: error
            })
        }
    }

}

module.exports = new TicketController;