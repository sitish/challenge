const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const {
  user,
} = require('../../models');
const bcrypt = require('bcrypt');

passport.use(
  'signup',
  new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      try {
        let userCreated = await user.create({
          name: req.body.name,
          email: email,
          password: password,
          image:"https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png",
          role:"user"
        });

        return done(null, userCreated, {
          message: "Signup success!"
        });
      } catch (e) {
        return done(e, false, {
          message: "Can't signup!"
        })
      }
    }
  )
);

passport.use(
  'signin',
  new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true

    },
    async (req, email, password, done) => {
      try {
        let userLogin = await user.findOne({
          where: {
            email: email
          }
        });

        if (!userLogin) {
          return done(null, false, {
            message: "User isn't found!"
          })
        }

        let validate = await bcrypt.compare(password, userLogin.password);

        if (!validate) {
          return done(null, false, {
            message: "Wrong password!"
          })
        }

        return done(null, userLogin, {
          message: "Signin success!"
        })
      } catch (e) {
        return done(e, false, {
          message: "Can't signin!"
        })
      }
    }
  )
)

// USER AUTH
passport.use(
  'user',
  new JWTstrategy({
          secretOrKey: 'secret_password', // key for jwt
          jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
      },
      async (token, done) => {

          const userLogin = await user.findOne({
              where:{email: token.user.email}
          })

          if (userLogin.role.includes("user")) {
              return done(null, token.user)
          }

                  return done(null, false)

      }
  )
)
passport.use(
  'admin',
  new JWTstrategy({
      secretOrKey: 'secret_password',
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      passReqToCallback: true
    },
    async (req, token, done) => {
      const userLogin = await user.findOne({
        where: {
          id: token.user.id
        },
      })
      if (userLogin.role.includes("admin")) {
        return done(null, userLogin)
      }
      return done(null, false)
    }
  )
)




