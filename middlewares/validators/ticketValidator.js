const {
    check,
    validationResult,
    matchedData,
    sanitize
  } = require('express-validator'); //form validation & sanitize form params
  const {
ticket
  } = require('../../models');

  
  module.exports = {
    create: [
      check('id_user').custom(value => {
        if (value) {
          return user.findOne({
            where:{
                id:value
            }
          }).then(result => {
            if (!result) {
              throw new Error("ID user doesn't exist")
            }
          })
        }
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          })
        }
        next();
      }
    ],
    closing: [
        check('id').custom(value => {
          if (value) {
            return ticket.findOne({
              where:{
                  id:value
              }
            }).then(result => {
              if (!result) {
                throw new Error("ID ticket doesn't exist")
              }
            })
          }
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            })
          }
          next();
        }
      ],
  }