'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ticket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  ticket.init({
    id_user: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    due_date: DataTypes.DATE,
    priority: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ticket',
    paranoid: true,
    timestamps: true,
    freezeTableName: true,

  });
  return ticket;
};