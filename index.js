const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const userRoutes = require('./routes/userRoutes');
const ticketRoutes = require('./routes/ticketRoutes');

const app = express();

app.use(passport.initialize());
app.use(passport.session())

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// app.use(express.static('public'));
app.use(express.static(__dirname + '/public'));
app.use(cors())

app.use('/user', userRoutes);
app.use('/ticket', ticketRoutes);

app.listen(3001, () => {
  console.log('App running on port 3001');
})

module.exports = app;
