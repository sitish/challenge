'use strict';
const bcrypt=require("bcrypt")
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('user', [{
      name: "Admin",
      email: "admin@gmail.com",
      role:"admin",
      password: bcrypt.hashSync("1234567890", 10),
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user', null, {});
  }
};